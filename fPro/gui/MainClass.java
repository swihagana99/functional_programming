package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import experiments.Experiment01;
import experiments.Experiment02;
import experiments.Experiment03;
import experiments.Experiment04;
import experiments.Experiment05;
import experiments.Experiment06;
import experiments.Experiment07;
import experiments.Experiment08;

public class MainClass extends JFrame implements ActionListener {
    private static final long serialVersionUID = 1L;
    static JButton button;
    static JLabel label;
    static JFrame frame = new JFrame("Functional Programming - TASK 05 GUI");
    static JPanel panel;

    public static void main(String[] args) {
        MainClass gui = new MainClass();
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(Box.createVerticalStrut(1));

        // Create the buttons
        button = createButton("Rugby Game Marks Foreach");
        panel.add(button);
        button.addActionListener(gui);

        panel.add(Box.createVerticalStrut(10));
        panel.add(Box.createVerticalStrut(1));

        button = createButton("Rugby Game Marks Parallel Streaming");
        panel.add(button);
        button.addActionListener(gui);

        panel.add(Box.createVerticalStrut(10));
        panel.add(Box.createVerticalStrut(1));

        button = createButton("Rugby Game Marks Lowest");
        panel.add(button);
        button.addActionListener(gui);

        panel.add(Box.createVerticalStrut(10));
        panel.add(Box.createVerticalStrut(1));

        button = createButton("Rugby Game Marks Filter");
        panel.add(button);
        button.addActionListener(gui);

        panel.add(Box.createVerticalStrut(10));
        panel.add(Box.createVerticalStrut(1));

        button = createButton("Rugby Game Marks Sorting");
        panel.add(button);
        button.addActionListener(gui);

        panel.add(Box.createVerticalStrut(10));
        panel.add(Box.createVerticalStrut(1));

        button = createButton("Rugby Game Marks Reduce");
        panel.add(button);
        button.addActionListener(gui);

        panel.add(Box.createVerticalStrut(10));
        panel.add(Box.createVerticalStrut(1));

        button = createButton("Rugby Game Club Names Collect");
        panel.add(button);
        button.addActionListener(gui);

        panel.add(Box.createVerticalStrut(10));
        frame.setSize(450, 360);
        frame.add(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        if (s.equals("Rugby Game Marks Foreach")) {
            Experiment01.readAndWrite();
        } else if (s.equals("Rugby Game Marks Parallel Streaming")) {
            Experiment02.readAndWriteParallel();
        } else if (s.equals("Rugby Game Marks Lowest")) {
            Experiment03.readAndWrite();
        } else if (s.equals("Rugby Game Marks Filter")) {
            Experiment04.readAndWrite();
        } else if (s.equals("Rugby Game Marks Sorting")) {
            Experiment05.readAndWrite();
        } else if (s.equals("Rugby Game Marks Reduce")) {
            Experiment06.readAndWrite();
        } else if (s.equals("Rugby Game Club Names Collect")) {
            Experiment07.readAndWrite();
            Experiment08.readAndWrite();
        }
    }

    private static JButton createButton(String text) {
        JButton button = new JButton(text);
        button.setFont(new Font("Arial", Font.BOLD, 14));
        button.setForeground(Color.BLACK);
        button.setBackground(Color.ORANGE);
        button.setPreferredSize(new Dimension(250, 40));
        return button;
    }
}
