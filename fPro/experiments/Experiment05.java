package experiments;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Experiment05 {
    public static void main(String args[]) {
        readAndWrite();
    }

    public static void readAndWrite() {
        List<String[]> table = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("Data/MyData.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                table.add(values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Printing the values using forEach method
        table.forEach(row -> {
            for (String value : row) {
                System.out.print(value + "\t"); // Iterating over each value in a row
            }
            System.out.println(); // Moving to the next line after printing a row
        });

        // Sort the table by the third column (won games)
        table.sort(Comparator.comparingInt(row -> Integer.parseInt(row[3])));

        try {
            FileWriter writer = new FileWriter("Data/ClubDataForEach.txt");

            // Writing the sorted values to the file using forEach method
            table.forEach(row -> {
                for (String value : row) {
                    try {
                        writer.write(value + "\t"); // Iterating over each value in a row and writing to the file
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    writer.write("\n"); // Moving to the next line after writing a row
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            writer.close();
            System.out.println("Successfully created club marks for each (sorted by won games)!");
        } catch (IOException e) {
            System.out.println("Try Again!");
            e.printStackTrace();
        }
    }
}
