package experiments;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Experiment06 {
    public static void main(String args[]) {
        readAndWrite();
    }

    public static void readAndWrite() {
        List<String[]> table = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("Data/MyData.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                table.add(values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Printing the values using forEach method
        table.forEach(row -> {
            Arrays.stream(row).forEach(value -> System.out.print(value + "\t"));
            System.out.println();
        });

        try {
            FileWriter writer = new FileWriter("Data/ClubDataForEach.txt");

            // Writing the values to the file using forEach method
            table.forEach(row -> {
                Arrays.stream(row).forEach(value -> {
                    try {
                        writer.write(value + "\t");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                try {
                    writer.write("\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            writer.close();
            System.out.println("Successfully used Lambda Function!");
        } catch (IOException e) {
            System.out.println("Try Again!");
            e.printStackTrace();
        }
    }
}
