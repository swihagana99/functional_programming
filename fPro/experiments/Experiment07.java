package experiments;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Experiment07 {
    public static void main(String args[]) {
        readAndWrite();
    }

    public static void readAndWrite() {
        List<String[]> table = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("Data/MyData.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                table.add(values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Printing the values using forEach method
        table.forEach(row -> {
            for (String value : row) {
                System.out.print(value + "\t"); // Iterating over each value in a row
            }
            System.out.println(); // Moving to the next line after printing a row
        });

        try {
            FileWriter writer = new FileWriter("Data/ClubDataForEach.txt");

            // Writing the values to the file using forEach method
            table.forEach(row -> {
                for (String value : row) {
                    try {
                        writer.write(value + "\t"); // Iterating over each value in a row and writing to the file
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    writer.write("\n"); // Moving to the next line after writing a row
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            writer.close();
            System.out.println("Successfully reduce function used!");

            // Calculate total points using reduce method
            Optional<Integer> totalPoints = table.stream()
                    .map(row -> Integer.parseInt(row[12].trim())) // Extract points value from each row
                    .reduce(Integer::sum); // Sum all the points
            System.out.println("Total points: " + totalPoints.orElse(0));

        } catch (IOException e) {
            System.out.println("Try Again!");
            e.printStackTrace();
        }
    }
}
